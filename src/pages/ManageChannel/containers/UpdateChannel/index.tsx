import { Card } from "antd";

import ManualCreate from "../../components/ManualCreate";

const UpdateChannel = () => {
  return (
    <Card bordered={false} className="channel-content-wrapper">
      <ManualCreate />
    </Card>
  );
};

export default UpdateChannel;
