import { Breadcrumb, Card } from "antd";
import React, { useState } from "react";
import AutoCreate from "../../components/AutoCreate";
import ManualCreate from "../../components/ManualCreate";

const CreateChannel = () => {
  const [activeTabKey, setActiveTabKey] = useState<string>("auto");
  const tabListNoTitle = [
    {
      key: "auto",
      label: "Thêm kênh tự động",
    },
    {
      key: "manual",
      label: "Thêm kênh thủ công",
    },
  ];

  const contentListNoTitle: Record<string, React.ReactNode> = {
    auto: <AutoCreate />,
    manual: <ManualCreate />,
  };

  const onTabChange = (key: string) => {
    setActiveTabKey(key);
  };

  return (
    <div>
      <Breadcrumb
        items={[
          {
            title: "Home",
          },
          {
            title: "Quản lý kênh",
          },
          {
            title: "Thêm mới kênh",
          },
        ]}
        style={{ display: "inline-flex", fontSize: 14 }}
      />
      <Card
        bordered={false}
        style={{ marginTop: 16 }}
        tabList={tabListNoTitle}
        onTabChange={onTabChange}
        activeTabKey={activeTabKey}
      >
        {contentListNoTitle[activeTabKey]}
      </Card>
    </div>
  );
};

export default CreateChannel;
