import { Form, Input, Modal, Select } from "antd";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { useAppDispatch } from "~/store/configureStore";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { LinkChanel } from "~/types/linkChanel.type";
import { addLinkChanel, getLinkChanel } from "./linkChanelSlice";
import React from "react";
import FormItem from "~/components/FormItem";

interface Props {
  open: boolean;
  onClose: () => void;
}

const linkSchema = yup.object().shape({
  link: yup
    .string()
    .required("Trường này không được để trống")
    .matches(/\.m3u8$/, "Đường dẫn này không hợp lệ")
    .matches(/^https?:\/\//i, "Đây không phải là một đường dẫn"),
  status: yup
    .number()
    .required("Status is required")
    .oneOf([0, 1], "Status must be either 0 or 1"),
});

const CreateLinkModal: React.FC<Props> = ({ open, onClose }: Props) => {
  const dispatch = useAppDispatch();

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<any>({
    resolver: yupResolver(linkSchema),
  });

  const onSubmit: SubmitHandler<LinkChanel> = (data) => {
    if (data) {
      try {
        dispatch(addLinkChanel(data));
        dispatch(getLinkChanel());
        onClose();
      } catch (error) {
        console.log("🚀 ~ file: index.tsx:21 ~ onSubmit ~ error:", error);
      }
    }
  };

  return (
    <>
      <Modal
        open={open}
        onCancel={onClose}
        onOk={handleSubmit(onSubmit)}
        title={"Thêm mới đường dẫn"}
      >
        <Form onFinish={handleSubmit(onSubmit)}>
          <FormItem title="Đường dẫn" errorText={errors?.link?.message}>
            <Controller
              name="link"
              control={control}
              render={({ field }) => (
                <Input placeholder="Nhập đường dẫn kênh" {...field} />
              )}
            />
          </FormItem>
          <FormItem required title={"Trạng thái"}>
            <Controller
              name="status"
              control={control}
              defaultValue={1}
              render={({ field }) => (
                <Select
                  {...field}
                  placeholder="Lựa chọn trạng thái đường dẫn"
                  options={[
                    { value: 1, label: "Hoạt động" },
                    { value: 0, label: "Không hoạt động" },
                  ]}
                />
              )}
            />
          </FormItem>
        </Form>
      </Modal>
    </>
  );
};

export default CreateLinkModal;
