import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { database } from "~/firebase-app/firebase";
import { LinkChanel } from "~/types/linkChanel.type";
import { onValue, push, ref, remove, set } from "firebase/database";

interface LinkChanelState {
  linkList: LinkChanel[];
  loading: boolean;
  error: null | string;
}

const initialState = {
  linkList: [],
  loading: false,
  error: null,
} as LinkChanelState;

export const addLinkChanel = createAsyncThunk(
  "linkChanel/addLinkChanel",
  async (data: LinkChanel, thunkApi) => {
    try {
      const linkRef = ref(database, "link-chanel");
      const newLinkRef = push(linkRef);
      set(newLinkRef, {
        ...data,
        id: newLinkRef.key,
        createAt: new Date().toISOString(),
      });
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const getLinkChanel = createAsyncThunk<LinkChanel[], void>(
  "linkChanel/getLinkChanel",
  async (_, thunkApi) => {
    try {
      const linkRef = ref(database, "link-chanel");

      return new Promise<LinkChanel[]>((resolve) => {
        onValue(
          linkRef,
          (snapshot) => {
            const result: LinkChanel[] = [];
            snapshot.forEach((childSnapshot) => {
              const childData = childSnapshot.val();
              result.push({
                ...childData,
                id: childSnapshot.key,
              });
            });
            resolve(result);
          },
          {
            onlyOnce: true,
          }
        );
      });
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const deleteLinkChanel = createAsyncThunk(
  "linkChanel/deleteLinkChanel",
  async (linkId: string, thunkApi) => {
    const dataRef = ref(database, "link-chanel/" + linkId);
    try {
      await remove(dataRef);
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const deleteAllLinkChanel = createAsyncThunk(
  "linkChanel/deleteAllLinkChanel",
  async (_, thunkApi) => {
    try {
      const dataRef = ref(database, "link-chanel");
      await remove(dataRef);
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

const linkChanelSlice = createSlice({
  name: "linkChanel",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addLinkChanel.pending, (state) => {
        state.loading = true;
      })
      .addCase(addLinkChanel.fulfilled, (state, action: PayloadAction<any>) => {
        state.loading = false;
        state?.linkList?.push(action.payload);
      })
      .addCase(addLinkChanel.rejected, (state, action: any) => {
        state.loading = false;
        state.error = action.payload;
      })
      .addCase(getLinkChanel.fulfilled, (state, action: any) => {
        state.loading = false;
        state.linkList = action.payload;
      })
      .addCase(deleteLinkChanel.fulfilled, (state, action: any) => {
        state.loading = false;
        state.linkList = state.linkList.filter(
          (item) => item.id !== action.payload
        );
      })
      .addCase(deleteAllLinkChanel.fulfilled, (state) => {
        state.loading = false;
      });
  },
});

export default linkChanelSlice.reducer;
