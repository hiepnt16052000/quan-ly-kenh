import {
  ArrowLeftOutlined,
  PlusOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { Button, Flex, Upload, UploadProps } from "antd";

const { Dragger } = Upload;

const AutoCreate = () => {
  const props: UploadProps = {
    name: "file",
    multiple: false,
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      console.log(info);

      //   if (status === "done") {
      //     message.success(`${info.file.name} file uploaded successfully.`);
      //   } else if (status === "error") {
      //     message.error(`${info.file.name} file upload failed.`);
      //   }
    },
    onDrop(e) {
      console.log("Dropped files", e.dataTransfer.files);
    },
    beforeUpload: () => false,
  };
  return (
    <Flex vertical gap={16}>
      <Dragger {...props}>
        <p className="ant-upload-drag-icon">
          <UploadOutlined />
        </p>
        <p className="ant-upload-text">
          Click or drag file to this area to upload
        </p>
      </Dragger>
      <Flex gap={8}>
        <Button icon={<PlusOutlined />} type="primary">
          Thêm mới
        </Button>
        <Button icon={<ArrowLeftOutlined />}>Quay lại</Button>
      </Flex>
    </Flex>
  );
};

export default AutoCreate;
