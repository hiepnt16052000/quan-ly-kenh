import { EditOutlined, PlusOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import FormItem from "~/components/FormItem";
import CreateLinkModal from "../CreateLinkModal";
import { Chanel } from "~/types/chanel.type";
import { addChanel } from "./chanelSlice";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { LinkChanel } from "~/types/linkChanel.type";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import dayjs from "dayjs";
import {
  Button,
  Divider,
  Flex,
  Form,
  Input,
  Popconfirm,
  Table,
  Tag,
  Typography,
  notification,
} from "antd";
import {
  IRootState,
  useAppDispatch,
  useAppSelector,
} from "~/store/configureStore";
import {
  deleteAllLinkChanel,
  deleteLinkChanel,
  getLinkChanel,
} from "../CreateLinkModal/linkChanelSlice";
import type { NotificationPlacement } from "antd/es/notification/interface";

const chanelSchema = yup.object().shape({
  nameChanel: yup.string().required("Không được để trống trường này"),
  linkLogo: yup
    .string()
    .required("Đường dẫn logo không được bỏ trống")
    .matches(/\.(png|jpg)$/, "Đường dẫn phải là ảnh")
    .matches(/^https?:\/\//i, "Đường dẫn phai không chính xác"),
});

const ManualCreate = () => {
  const dispatch = useAppDispatch();
  const linkList = useAppSelector((state: IRootState) => state.link.linkList);
  const [api, contextHolder] = notification.useNotification();
  const [openCreateLink, setOpenCreateLink] = useState(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  console.log("🚀 ~ ManualCreate ~ openModal:", openModal);
  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm<any>({ resolver: yupResolver(chanelSchema) });

  const linkTableColumns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
    },
    {
      title: "Đường dẫn",
      dataIndex: "link",
      key: "link",
    },
    {
      title: "Trạng thái",
      key: "status",
      dataIndex: "status",
      render: (status: 1 | 0) => {
        if (status === 1) {
          return <Tag color="green">Hoạt động</Tag>;
        } else {
          return <Tag color="volcano">Không hoạt động</Tag>;
        }
      },
    },
    {
      title: "Ngày tạo",
      dataIndex: "createAt",
      key: "createAt",
      render: (createAt: string) => {
        const date = new Date(createAt);
        const newDate = dayjs(date).format("DD/MM/YYYY, h:mm A");
        return <span>{newDate}</span>;
      },
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      render: (_: any, record: LinkChanel) => (
        <Flex gap={16}>
          <Button icon={<EditOutlined />}>Chỉnh sửa</Button>
          <Popconfirm
            title="Delete the task"
            description="Are you sure to delete this task?"
            onConfirm={() => handleDelete(record.id)}
            onCancel={handleCancel}
            okText="Yes"
            cancelText="No"
          >
            <Button danger>Delete</Button>
          </Popconfirm>
        </Flex>
      ),
    },
  ];

  const handleRestForm = () => {
    reset();
  };

  const handleDelete = (linkId: any) => {
    dispatch(deleteLinkChanel(linkId));
    dispatch(getLinkChanel());
    openNotification("topRight", "SUCCSSFULL", "Xóa link chanel thành công");
  };

  const openNotification = (
    placement: NotificationPlacement,
    message: string,
    description: string
  ) => {
    api.info({
      message: `${message}`,
      description: `${description}`,
      placement,
    });
  };

  const onSubmit: SubmitHandler<Chanel> = (data) => {
    if (linkList.length > 0) {
      try {
        dispatch(addChanel({ ...data, linkList }));
        dispatch(deleteAllLinkChanel());
        dispatch(getLinkChanel());
        openNotification("topRight", "SUCCSSFULL", "Thêm chanel thành công");
        handleRestForm();
      } catch (error) {
        console.log("🚀 ~ file: index.tsx:21 ~ onSubmit ~ error:", error);
      }
    } else {
      openNotification("topRight", "ERROR", "Cần thêm ít nhất một đường dẫn");
    }
  };

  const handleCancel = () => {
    setOpenModal(false);
  };

  useEffect(() => {
    dispatch(getLinkChanel());
  }, [dispatch]);

  return (
    <>
      {contextHolder}
      <Flex vertical gap={16}>
        <Form onFinish={handleSubmit(onSubmit)}>
          <Button
            type="primary"
            icon={<PlusOutlined />}
            className="btn-add"
            onClick={handleSubmit(onSubmit)}
          >
            Add
          </Button>
          <FormItem
            required
            title={"Tên kênh"}
            errorText={errors?.nameChanel?.message}
          >
            <Controller
              name="nameChanel"
              control={control}
              render={({ field }) => <Input {...field} />}
            />
          </FormItem>
          <FormItem
            required
            title={"Link logo"}
            errorText={errors?.linkLogo?.message}
          >
            <Controller
              name="linkLogo"
              control={control}
              render={({ field }) => <Input {...field} />}
            />
          </FormItem>
          <Divider style={{ margin: "4px 0" }} />
          <Flex align="center" gap={16}>
            <Typography.Title
              level={5}
              className="margin0"
              style={{ margin: 0 }}
            >
              Danh sách đường dẫn
            </Typography.Title>
            <Button
              icon={<PlusOutlined />}
              type="primary"
              onClick={() => setOpenCreateLink(true)}
            >
              Thêm mới
            </Button>
          </Flex>
          <Table
            columns={linkTableColumns}
            dataSource={linkList}
            rowKey={(record) => record?.id}
            // onRow={(record) => ({ onClick: () => handleDelete(record.id) })}
          />
          {openCreateLink && (
            <CreateLinkModal
              open={openCreateLink}
              onClose={() => setOpenCreateLink(false)}
            />
          )}
        </Form>
      </Flex>
    </>
  );
};

export default ManualCreate;
