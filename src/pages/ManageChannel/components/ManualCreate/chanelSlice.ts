import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { onValue, push, ref, remove, set } from "firebase/database";
import { database } from "~/firebase-app/firebase";
import { Chanel } from "~/types/chanel.type";

interface ChanelState {
  chanel: Chanel[];
  loading: boolean;
  error: null | string;
}

const initialState: ChanelState = {
  chanel: [],
  loading: false,
  error: null,
};

export const getChanel = createAsyncThunk(
  "chanel/getChanel",
  async (_, thunkApi) => {
    const chanelRef = ref(database, "chanel");
    try {
      const result = new Promise<Chanel[]>((resovel) => {
        onValue(
          chanelRef,
          (snapshot) => {
            const result: Chanel[] = [];
            snapshot.forEach((childSnapshot) => {
              const childKey = childSnapshot.key;
              const childData = childSnapshot.val();
              result.push({ ...childData, id: childKey });
            });
            resovel(result);
          },
          {
            onlyOnce: true,
          }
        );
      });
      console.log("🚀 ~ result ~ result:", result);

      return result;
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const addChanel = createAsyncThunk(
  "chanel/addChanel",
  async (data: Chanel, thunkApi) => {
    const chanelRef = ref(database, "chanel");
    try {
      const newChanelRef = push(chanelRef);
      set(newChanelRef, { ...data, createAt: new Date().toISOString() });
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

export const removeChanel = createAsyncThunk(
  "chanel/removeChanel",
  async (chanelId: string, thunkApi) => {
    const chanelRef = ref(database, "chanel/" + chanelId);
    try {
      await remove(chanelRef);
    } catch (error: any) {
      return thunkApi.rejectWithValue(error.message);
    }
  }
);

const chanelSlice = createSlice({
  name: "chanel",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addChanel.fulfilled, (state, action: PayloadAction<any>) => {
        state.loading = false;
        state.chanel.push(action.payload);
      })
      .addCase(getChanel.fulfilled, (state, action: PayloadAction<any>) => {
        state.loading = false;
        state.chanel = action.payload;
      })
      .addCase(removeChanel.fulfilled, (state, action: PayloadAction<any>) => {
        (state.loading = false),
          (state.chanel = state.chanel.filter(
            (item) => item.id !== action.payload
          ));
      });
  },
});

export default chanelSlice.reducer;
