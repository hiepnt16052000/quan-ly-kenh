import { EditOutlined, PlusOutlined, SearchOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Card,
  Flex,
  Input,
  Popconfirm,
  Table,
  Tag,
  Typography,
  notification,
} from "antd";
import React, { useEffect, useState } from "react";
import "./manageChannel.less";
import { useNavigate } from "react-router";
import { paths } from "~/routes/path";
import dayjs from "dayjs";
import { useAppDispatch, useAppSelector } from "~/store/configureStore";
import { getChanel, removeChanel } from "./components/ManualCreate/chanelSlice";
import { Chanel } from "~/types/chanel.type";
import useDebounce from "~/hooks/useDebounce";
import type { NotificationPlacement } from "antd/es/notification/interface";

const ManageChannel = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [searchText, setSearchText] = useState<string>("");
  const [filteredResults, setFilteredResults] = useState<Chanel[]>([]);
  const [api, contextHolder] = notification.useNotification();
  const [openModal, setOpenModal] = useState<boolean>(false);
  console.log("🚀 ~ ManageChannel ~ openModal:", openModal);

  const debounceValue = useDebounce(searchText);

  const chanelColumns = [
    {
      title: "Hình ảnh",
      dataIndex: "linkLogo",
      key: "linkLogo",
      render: (linkLogo: string) => (
        <img style={{ width: 100 }} src={linkLogo} />
      ),
    },
    {
      title: "Tên kênh",
      dataIndex: "nameChanel",
      key: "nameChanel",
      render: (nameChanel: string) => (
        <Typography.Link>{nameChanel}</Typography.Link>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "linkList",
      key: "linkList",
      render: (linkList: any) => {
        const search = linkList?.some((link: any) => link.status === 1);

        if (search) {
          return <Tag color="green">Hoạt động</Tag>;
        } else {
          return <Tag color="volcano">Không hoạt động</Tag>;
        }
      },
    },
    {
      title: "Ngày tạo",
      dataIndex: "createAt",
      key: "createAt",
      render: (createAt: string) => {
        const date = new Date(createAt);
        const newDate = dayjs(date).format("DD/MM/YYYY, h:mm A");
        return <p>{newDate}</p>;
      },
    },
    {
      title: "Action",
      key: "action",
      render: (_: any, record: Chanel) => (
        <Flex gap={16}>
          <Button icon={<EditOutlined />}>Chỉnh sửa</Button>
          <Popconfirm
            title="Delete the task"
            description="Are you sure to delete this task?"
            onConfirm={() => handleRemoveChanel(record.id)}
            onCancel={handleCancel}
            okText="Yes"
            cancelText="No"
          >
            <Button danger>Delete</Button>
          </Popconfirm>
        </Flex>
      ),
    },
  ];
  const chanelList = useAppSelector((state) => state.chanel.chanel);
  console.log("🚀 ~ ManageChannel ~ chanelList:", chanelList);

  const handleRemoveChanel = (chanelId: string) => {
    dispatch(removeChanel(chanelId));
    dispatch(getChanel());
    openNotification("topRight", "SUCCSSFULL", "Xóa link chanel thành công");
  };

  const handleCancel = () => {
    setOpenModal(false);
  };

  const openNotification = (
    placement: NotificationPlacement,
    message: string,
    description: string
  ) => {
    api.info({
      message: `${message}`,
      description: `${description}`,
      placement,
    });
  };

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    e.preventDefault();
    setSearchText(e.target.value);
  };

  useEffect(() => {
    const searchResult = () => {
      if (debounceValue) {
        const data = chanelList.filter((chanel) => {
          return chanel.nameChanel
            .toLowerCase()
            .includes(searchText.toLowerCase());
        });
        setFilteredResults(data);
      }
    };
    searchResult();
  }, [chanelList, debounceValue, searchText]);

  useEffect(() => {
    dispatch(getChanel());
  }, [dispatch]);

  return (
    <>
      {contextHolder}
      <div className="manage-channel">
        <Breadcrumb
          items={[
            {
              title: "Home",
            },
            {
              title: "Quản lý kênh",
            },
          ]}
          style={{ display: "inline-flex", fontSize: 14 }}
        />
        <Card bordered={false} className="channel-content-wrapper">
          <Flex justify="space-between" align="center">
            <Flex align="center" gap={16}>
              <Typography.Title level={5} className="margin0">
                Kênh truyền hình
              </Typography.Title>
              <Button
                type="primary"
                icon={<PlusOutlined />}
                onClick={() => navigate(paths.createChannel)}
              >
                Thêm mới
              </Button>
            </Flex>
            <Flex align="center" gap={16}>
              <Input
                placeholder="Nhập từ khoá"
                style={{ width: 280 }}
                onChange={handleOnChange}
              />
              <Button type="primary" icon={<SearchOutlined />}>
                Tìm kiếm
              </Button>
            </Flex>
          </Flex>
          <div className="channel-table">
            <Table
              columns={chanelColumns}
              dataSource={searchText ? filteredResults : chanelList}
              rowKey={(record) => record?.id}
            />
          </div>
        </Card>
      </div>
    </>
  );
};

export default ManageChannel;
