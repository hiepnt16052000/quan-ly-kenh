import { Layout, Menu, Typography } from "antd";
import { ItemType } from "antd/lib/menu/hooks/useItems";
import { Outlet, useLocation, useNavigate } from "react-router";
import { paths } from "~/routes/path";
import "./home.less";

const { Sider, Header, Content } = Layout;

type MenuItemType = ItemType & {
  isActive?: boolean;
};

const Home = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const sidebarItems: MenuItemType[] = [
    {
      key: paths.manageChannel,
      // icon: <UserOutlined />,
      label: "Quản lý kênh",
      onClick: () => navigate(paths.manageChannel),
    },
  ];

  return (
    <Layout hasSider style={{ minHeight: "100vh" }} className="home">
      <Sider
        trigger={null}
        collapsible
        style={{ position: "fixed", height: "100%" }}
        className="sidebar"
        width={240}
      >
        <div className="logo-app">
          <Typography.Text
            style={{ fontWeight: "bold", color: "white", fontSize: 24 }}
          >
            Logo
          </Typography.Text>
        </div>
        <Menu
          theme="dark"
          mode="inline"
          selectedKeys={[location.pathname]}
          style={{ fontSize: 16, color: "white" }}
          items={sidebarItems}
        />
      </Sider>
      <Layout style={{ marginLeft: "240px" }}>
        <Header style={{ backgroundColor: "#fff", padding: "0 20px" }}></Header>
        <Content>
          <div style={{ color: "#333", padding: 18, height: "100%" }}>
            <Outlet />
          </div>
        </Content>
      </Layout>
    </Layout>
  );
};

export default Home;
