import { combineReducers } from "@reduxjs/toolkit";
import linkChanelSlice from "~/pages/ManageChannel/components/CreateLinkModal/linkChanelSlice";
import chanelSlice from "~/pages/ManageChannel/components/ManualCreate/chanelSlice";
import { InjectedReducersType } from "~/utils/types/injector-typings";

export function createReducer(injectedReducers: InjectedReducersType = {}) {
  return combineReducers({
    ...injectedReducers,
    link: linkChanelSlice,
    chanel: chanelSlice,
  });
}
