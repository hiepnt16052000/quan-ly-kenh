import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Home from "~/pages/Home";
import { paths } from "./path";
import ManageChannel from "~/pages/ManageChannel";
import CreateChannel from "~/pages/ManageChannel/containers/CreateChannel";
import UpdateChannel from "~/pages/ManageChannel/containers/UpdateChannel";

export default function AppRoutes() {
  const routes = createBrowserRouter([
    {
      path: paths.home,
      element: <Home />,
      children: [
        { path: paths.manageChannel, element: <ManageChannel /> },
        {
          path: paths.createChannel,
          element: <CreateChannel />,
        },
        {
          path: paths.updateChannel,
          element: <UpdateChannel />,
        },
      ],
    },
  ]);
  return <RouterProvider router={routes} />;
}
