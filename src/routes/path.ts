export const paths = {
  home: "/",
  manageChannel: "/channel",
  createChannel: "/create-channel",
  updateChannel: "/update-channel/:id",
};
