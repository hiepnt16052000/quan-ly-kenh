import { Col, Row, Typography } from "antd";
import { memo } from "react";

interface Props {
  children?: any;
  title?: any;
  direction?: "horizontal" | "vertical";
  errorText?: any;
  required?: boolean;
}

// eslint-disable-next-line react-refresh/only-export-components
const FormItem = ({
  direction = "vertical",
  children,
  title,
  errorText,
  required,
}: Props) => {
  return (
    <Row>
      <Col
        span={direction === "vertical" ? 24 : 6}
        xs={24}
        md={24}
        lg={direction === "vertical" ? 24 : 6}
      >
        <Typography.Title level={5} className="margin0">
          {title}
          {required && (
            <span style={{ color: "red", marginLeft: "6px" }}>*</span>
          )}
        </Typography.Title>
      </Col>
      <Col
        span={direction === "vertical" ? 24 : 18}
        xs={24}
        md={24}
        lg={direction === "vertical" ? 24 : 18}
      >
        {children}
        {errorText && (
          <Typography.Text type="danger">{errorText}</Typography.Text>
        )}
      </Col>
    </Row>
  );
};

// eslint-disable-next-line react-refresh/only-export-components
export default memo(FormItem);
