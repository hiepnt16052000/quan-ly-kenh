export type LinkChanel = {
  id: string;
  link: string;
  status: number;
  createAt: string;
  order: number;
};
