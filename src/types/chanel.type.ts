import { LinkChanel } from "./linkChanel.type";

export type Chanel = {
  id: string;
  nameChanel: string;
  linkLogo: string;
  linkList: LinkChanel[] | undefined;
  createAt: string;
};
