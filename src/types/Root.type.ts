import { Chanel } from "./chanel.type";
import { LinkChanel } from "./linkChanel.type";

export interface RootState {
  link: LinkChanel[];
  chanel: Chanel[];
}
