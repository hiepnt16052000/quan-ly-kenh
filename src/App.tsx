import AppRoutes from "./routes";
import "./styles/index.less";

function App() {
  return (
    <>
      <AppRoutes />
    </>
  );
}

export default App;
