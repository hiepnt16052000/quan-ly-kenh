// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCl-UMri-GYz4_j1ywwRQRNX4mg4XydBug",
  authDomain: "quan-ly-kenh-b3e81.firebaseapp.com",
  databaseURL:
    "https://quan-ly-kenh-b3e81-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "quan-ly-kenh-b3e81",
  storageBucket: "quan-ly-kenh-b3e81.appspot.com",
  messagingSenderId: "760568394079",
  appId: "1:760568394079:web:d45863f434652e2a8706ea",
  measurementId: "G-HD5Q2X6KYP",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const database = getDatabase(app);
